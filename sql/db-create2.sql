CREATE TABLE users (id INT NOT NULL GENERATED ALWAYS AS IDENTITY,login VARCHAR(10), PRIMARY KEY (id))
CREATE TABLE teams (id INT NOT NULL GENERATED ALWAYS AS IDENTITY,name VARCHAR(10), PRIMARY KEY (id))
CREATE TABLE users_teams (user_id INT REFERENCES users(id) on delete cascade,team_id INT REFERENCES teams(id) on delete cascade, UNIQUE(user_id, team_id))
INSERT INTO users VALUES (DEFAULT, 'ivanov')
INSERT INTO teams VALUES (DEFAULT, 'teamA')
SELECT * FROM users
SELECT * FROM teams
SELECT * FROM users_teams
